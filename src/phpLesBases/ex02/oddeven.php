<?php

while (true){
    echo "Entrez un nombre: ";
    $nombre = rtrim(fgets(STDIN));

    if (is_numeric($nombre)) {
        if ($nombre % 2 == 0) {
            echo "Le chiffre $nombre est Pair\n";
        } else {
            echo "Le chiffre $nombre est Impair\n";
        }
    } else {
        echo "'$nombre' n'est pas un chiffre\n";
    }
}
