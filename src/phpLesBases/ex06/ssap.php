<?php
//Création d'une liste vide
$list=[];

// Récupérer la fonction de l'ex03 qui prend une chaine de caractères en argument, et renvoie un tableau trié des différents mots)

function ft_split($input) {
    $tableau = preg_split("/[\s]+/",$input,-1,PREG_SPLIT_NO_EMPTY);

    sort ($tableau);
    return $tableau;
}

// récupere le tableau
$tableau = array_slice ($argv,1);
// Fonction de trie
sort($tableau);

// je créée une boucle avec le nombre d'élèments
for ($i = 1; $i < $argc; $i++) {

    foreach (ft_split($argv[$i]) as $mot) 
     //je créé un tableau
   {
     array_push($list,$mot);}
 };

 // je trie tous les mots de mon tableau dans un ordre logique
    sort($list,SORT_STRING);

// j'appelle chaque valeur dans mon tableau et je la renvoie
foreach ($list as $value) 
{

    print_r($value);
    echo "\n";
};
// boucle avec affichage standart des mots