<?php
// 1 paramètre avec le calcul "valeur operateur valeur"
// afficher message d'erreur si c'est pas des chiffres 
// enlever les espaces

//aller sur regex, créer un calcul qui comprend toutes les valeurs.
//Utilisation de preg_match_all
// il faut casser la chaîne pour récupérer les paramètres
// il faut faire une valeur qui contient le calcul, 

// if ($argc !== 2) {
//     echo "Incorrect Parameters\n";
// } 


// isset détermine si la valeur est nulle (rien dedans),le ! détermine que ce n'est pas le cas justement, car argument 1 c'est le lien, c'est obligé d'avoir un lien. Pour le deuxieme isset on dit que si l'argument 2 donc "toto" ou "1+3" est nul, on affiche un message d'erreur. Ainsi, on détermine aussi qu'il y a que deux paramètre ici.
//Le exit affiche le statut


if (!isset($argv[1]) || isset($argv[2])) {
    echo "Incorrect Parameters\n";
    exit();
}

//Ici on récupère les éléments de la chaine en la cassant, en utilisant regex pour dire que SI la clef n1 doit être numérique en enlevant les espaces avant et après, ensuite je nomme l'opérateur et je liste ses possibilités avec des | pour dire OU. et je nomme la dernière clef en récupérant les memes paramètres que la 1. Toujours dans le regex, je détermine de quel arguemnt il s'agit, après je vais stocker les paramètres dans un tableau. Le PREG_SET_ORDER va afficher les résultats dans l'ordre.
// [\+-]?\d*\.?\d+? pour le bonus, il peut contenir des nombres positifs et négatifs, ou à virgule.

if (preg_match_all("/^\s*(?<num1>[\+-]?\d*\.?\d+?)\s*(?<ope>\+|\*|\/|\-|\%)\s*(?<num2>(?1))\s*$/", $argv[1], $tab, PREG_SET_ORDER)) {

// je nomme mes valeurs en allant les chercher dans les tableaux, d'abord le $tab et ensuite dans le 0, et je les prend en récupérant la clef associée.
    $var1=$tab[0]["num1"];
    $var2=$tab[0]["num2"];
    $ope=$tab[0]["ope"];

    if ($ope == "+") {
        echo $var1 + $var2."\n";  
    }

    if ($ope == "-") {
        echo $var1 - $var2."\n";  
    }

    if ($ope == "*") {
        echo $var1 * $var2."\n";  
    }

    if ($ope == "/") {
        if ($var2 == 0) {
            echo "0\n";
        } else {
            echo $var1 / $var2."\n";
        }
    }
   
    if ($ope == "%") {
        echo $var1 % $var2."\n";  
    }
} else {
    echo "Syntax Error\n";
}
    


